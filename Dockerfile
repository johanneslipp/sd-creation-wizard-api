FROM adoptopenjdk/openjdk11:alpine-jre
ARG JAR_FILE=target/creation-wizard-api.jar
COPY ${JAR_FILE} /app.jar
COPY shapes/ /shapes/
ENTRYPOINT ["java","-jar","/app.jar"]
