package eu.gaiax.sdcreationwizard.api.dto;

import java.util.List;
import java.util.Map;

public class CategorizedShapes {
    private final Map<String,Map<String, List<String>>> shapes;

    public CategorizedShapes(Map<String,Map<String, List<String>>> shapes) {
        this.shapes = shapes;
    }

    public Map<String,Map<String, List<String>>> getShapes() {
        return shapes;
    }

    public Map<String, List<String>> getEcosystem(String eco_system) {
        return shapes.get(eco_system);
    }

    public Map<String, List<String>> getDefaultEcosystem() {
        return shapes.get("trusted-cloud");
    }
}
