package eu.gaiax.sdcreationwizard.api.dto;

import java.io.File;
import java.util.Collection;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.text.WordUtils;
import org.apache.jena.base.Sys;
import org.apache.jena.ext.com.google.common.util.concurrent.CycleDetectingLockFactory;

public class Formaliser {

    private Collection<File> files;

    public Formaliser(Collection<File> files) {
        this.files = files;
    }

    public void formalise()  {

        for (File file: files) {

            boolean indicator = false;
            String name = file.getName();
            String save_name = file.getName();
            if (name.contains("Shape")){
                name = name.replaceAll("Shape", "");
                indicator = true;
            }

            // Remove Dash in between
            Pattern pattern = Pattern.compile("-", Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(name);
            boolean matchFound = matcher.find();
            if(matchFound) {
                // replace - with Space " "
                name = name.replaceAll("-", " ");
                // Capitalise each word

                indicator = true;
            }

            // Capitalise each word
            name = WordUtils.capitalize(name);

            if (indicator == true) {
                Path source = Paths.get(file.getPath());
                //System.out.println(source);
                try {
                    System.out.println("Renaming " + save_name + " to " + name );
                    Files.move(source, source.resolveSibling(name));

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
