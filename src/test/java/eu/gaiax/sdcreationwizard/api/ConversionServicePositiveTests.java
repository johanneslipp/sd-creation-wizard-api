package eu.gaiax.sdcreationwizard.api;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
@WebAppConfiguration
@ContextConfiguration(classes = VicShapeProcessorApplication.class)
public class ConversionServicePositiveTests {

    @ClassRule
    public static final SpringClassRule scr = new SpringClassRule();
    final static String CONVERT_FILE_URL = "/convertFile";
    @Rule
    public final SpringMethodRule smr = new SpringMethodRule();
    @Parameter(value = 0)
    public String inputFilename;
    @Parameter(value = 1)
    public String outputFilename;
    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Parameters(name = "{0}")
    public static Collection<Object[]> filenames() {
        return Arrays.asList(new Object[][]{

                //0. Preliminary test for converting shacl shapes to json object
                {"input-shacl-file.ttl", "output.json"},

                //1. sh:datatype in shacl file
                {"test-datatype.ttl", "test-datatype-output.json"},

                //2. custom datatypes in shacl file
                 {"test-datatype2.ttl", "test-datatype2-output.json"},

                //3. sh:description in shacl file
                        {"test-description.ttl", "test-description-output.json"},

                //4. sh:group in shacl file
                {"test-group.ttl", "test-group-output.json"},

                //5. sh:in in shacl file with a list of strings
                {"test-in-property.ttl", "test-in-property-output.json"},

                //6. sh:in handling for different types
                {"test-in-property2.ttl", "test-in-property2-output.json"},

                //7. sh:maxcount in shacl file
                {"test-max-count.ttl", "test-max-count-output.json"},

                //8. sh:mincount in shacl file
                {"test-min-count.ttl", "test-min-count-output.json"},

                //9. sh:maxinclusive in shacl file
                 {"test-max-inclusive.ttl", "test-max-inclusive-output.json"},

                //10. sh:maxinclusive in shacl file with negative value
                {"test-negative-max-inclusive.ttl", "test-negative-max-inclusive-output.json"},

                //11. sh:mininclusive in shacl file
                {"test-min-inclusive.ttl", "test-min-inclusive-output.json"},

                //12. sh:mininclusive in shacl file with negative value
                {"test-negative-min-inclusive.ttl", "test-negative-min-inclusive-output.json"},

                //13. sh:maxlengtht in shacl file
                        {"test-max-length.ttl", "test-max-length-output.json"},

                //14. sh:maxlengtht in shacl file with negative value
                 {"test-negative-max-length.ttl", "test-negative-max-length-output.json"},

                //15. sh:minlength in shacl file
                 {"test-min-length.ttl", "test-min-length-output.json"},

                //16. sh:minlength in shacl file with negative value
                 {"test-negative-min-length.ttl", "test-negative-min-length-output.json"},

                //17. sh:order in shacl file
                 {"test-order.ttl", "test-order-output.json"},

                //18. sh:order in shacl file with negative value
                {"test-negative-order.ttl", "test-negative-order-output.json"},

                //19. sh:path in shacl file
                 {"test-path.ttl", "test-path-output.json"},

                //20. multiple not connected shapes in a shacl file
               {"test-multiple-shapes.ttl", "test-multiple-shapes-output.json"},

                //21. nested shapes in a shacl file
                {"test-nested-shapes.ttl", "test-nested-shapes-output.json"},

                //22. sh:name in shacl file
                {"test-name.ttl", "test-name-output.json"},

                //23. sh:minExclusive in shacl file
                {"test-min-exclusive.ttl", "test-min-exclusive-output.json"},

                //24. sh:minExclusive in shacl file with -ve value
                {"test-negative-min-exclusive.ttl", "test-negative-min-exclusive-output.json"},

                //25. sh:maxExclusive in shacl file
                 {"test-max-exclusive.ttl", "test-max-exclusive-output.json"},

                //26. sh:maxExclusive in shacl file with negative value
                {"test-negative-max-exclusive.ttl", "test-negative-max-exclusive-output.json"},

                //27. sh:class in shacl file
                {"test-class.ttl", "test-class-output.json"},

                //28. sh:or in shacl file with mincount
               {"test-sh-or-mincount.ttl", "test-sh-or-mincount-output.json"},

                //29. sh:or in shacl file with maxcount
                {"test-sh-or-maxcount.ttl", "test-sh-or-maxount-output.json"},

                //30. sh:or in shacl file with datatype and class
                {"test-sh-or.ttl", "test-sh-or-output.json"},

                //31. unhandled/unsupported properties in shacl file
                {"test-unhandled-properties.ttl", "test-unhandled-properties-output.json"}
        }
        );
    }

    @Before
    public void setUp() {
        DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
        this.mockMvc = builder.build();
        Mockito.mock(ConversionController.class);

    }

    /**
     * method that matches output jsons returned from
     * ConversionService.convertToJson with expected output jsons stored in
     * src/test/java/resources. Input to the method are input shacl filenames and
     * output json filenames stored in test/resources. This method is called from
     * each individual test case involving validating output jsons
     */
    @Test
    public void testOutputJsons() throws Exception {

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream inputFile = classloader.getResourceAsStream(inputFilename);
        ResultMatcher ok = MockMvcResultMatchers.status().isOk();
        String outputFile = IOUtils.toString(classloader.getResourceAsStream(outputFilename), StandardCharsets.UTF_8);
        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", inputFilename,
                MediaType.MULTIPART_FORM_DATA_VALUE, inputFile);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.multipart(CONVERT_FILE_URL)
                .file(mockMultipartFile);
        MvcResult mvcResult = mockMvc.perform(builder).andExpect(ok).andReturn();

        JSONAssert.assertEquals(outputFile, mvcResult.getResponse().getContentAsString(), false);
    }

}
